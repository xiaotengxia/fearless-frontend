import React from 'react';

class ConferenceForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            starts: '',
            ends: '',
            description: '',
            max_presentations: '',
            max_attendees: '',
            locations: []
        }
        
        this.handleFieldChange = this.handleFieldChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.locations;
        // console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);
            
            const cleared = {
                name: '',
                starts: '',
                ends: '',
                description: '',
                max_presentations: '',
                max_attendees: '',
                location: '',

            };
            this.setState(cleared);
        }
      }


    handleFieldChange(event) {
        const value = event.target.value;
        this.setState({[event.target.id]: value})
    }


    async componentDidMount() {
        const url = "http://localhost:8000/api/locations/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({locations: data.locations});
            // const selectTag = document.getElementById("location");
            // for (let location of data.locations) {
            //     const locationName = location.name;
            //     const locationId = location.id;
            //     // console.log(locationId);
            //     const optionTag = document.createElement("option");
            //     optionTag.value = locationId;
            //     optionTag.innerHTML = locationName;
            //     selectTag.appendChild(optionTag);
            // }
        };
    }


    render() {
        return (
            <div className="row">
              <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new conference</h1>
                    <form onSubmit={this.handleSubmit} id="create-conference-form">

                    <div className="form-floating mb-3">
                        <input onChange={this.handleFieldChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={this.state.name}/>
                        <label htmlFor="name">Name</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input onChange={this.handleFieldChange} placeholder="MM/DD/YYYY" required type="date" name="starts" id="starts" className="form-control" value={this.state.starts}/>
                        <label htmlFor="starts">Start date</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleFieldChange} placeholder="MM/DD/YYYY" required type="date" name="ends" id="ends" className="form-control" value={this.state.ends}/>
                        <label htmlFor="ends">End date</label>
                    </div>

                    <div className="mb-3">
                        <label htmlFor="description">Description</label>
                        <textarea onChange={this.handleFieldChange} placeholder="Description" required type="text" name="description" id="description" className="form-control" rows="4" cols="50" value={this.state.description}></textarea>
                        
                    </div>

                    <div className="form-floating mb-3">
                        <input onChange={this.handleFieldChange} placeholder="Max presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" value={this.state.max_presentations}/>
                        <label htmlFor="max_presentations">Max presentations</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleFieldChange} placeholder="Max attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" value={this.state.max_attendees}/>
                        <label htmlFor="max_attendees">Max attendees</label>
                    </div>
                
                    <div className="mb-3">
                        <select onChange={this.handleFieldChange} required name="location" id="location" className="form-select" value={this.state.location}>
                            <option value="">Choose a Location</option>
                            {this.state.locations.map(location => {
                                return (
                                <option key={location.id} value={location.id}>
                                    {location.name}
                                </option>
                                );
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
              </div>
            </div>
        )
        }

}



export default ConferenceForm;