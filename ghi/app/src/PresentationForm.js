import React from 'react';

class PresentationForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      company_name: '',
      presenter_email: '',
      presenter_name: '',
      synopsis: '',
      title: '',
      conferences: []
    }

    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.conferences;
    // console.log(data);

    const presentationUrl = 'http://localhost:8000/api/presentations/';
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(presentationUrl, fetchConfig);
    if (response.ok) {
      const newPresentation = await response.json();
      console.log(newPresentation);

      const cleared = {
          company_name: '',
          conference: '',
          presenter_email: '',
          presenter_name: '',
          synopsis: '',
          title: '',
        
        };
      this.setState(cleared);
    }
}

  handleFieldChange(event) {
    const value = event.target.value;
    this.setState({[event.target.id]: value})
  }

  async componentDidMount() {
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      // console.log(data)
      this.setState({conferences: data.conferences});
    }
  }

  render() {
      return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new presentation</h1>
              <form onSubmit={this.handleSubmit} id="create-presentation-form">

                <div className="form-floating mb-3">
                  <input onChange={this.handleFieldChange} placeholder="Presenter Name" required type="text" name="presenter_name" id="presenter_name" className="form-control" value={this.state.presenter_name}/>
                  <label htmlFor="presenter_name">Presenter Name</label>
                </div>

                <div className="form-floating mb-3">
                  <input onChange={this.handleFieldChange} placeholder="Presenter Email" required type="email" name="presenter_email" id="presenter_email" className="form-control" value={this.state.presenter_email}/>
                  <label htmlFor="presenter_email">Presenter Email</label>
                </div>
              
                <div className="form-floating mb-3">
                  <input onChange={this.handleFieldChange} placeholder="Company Name" required type="text" name="company_name" id="company_name" className="form-control" value={this.state.company_name}/>
                  <label htmlFor="company_name">Company Name</label>
                </div>

                <div className="form-floating mb-3">
                  <input onChange={this.handleFieldChange} placeholder="Title" required type="text" name="title" id="title" className="form-control" value={this.state.title}/>
                  <label htmlFor="title">Title</label>
                </div>

                <div className="form-floating mb-3">
                  <textarea onChange={this.handleFieldChange}  placeholder="Synopsis" required type="text" name="synopsis" id="synopsis" className="form-control" rows="3" cols="50" value={this.state.synopsis}></textarea>
                  <label htmlFor="synopsis">Synopsis</label>
                </div>
            
                <div className="mb-3">
                  <select onChange={this.handleFieldChange} required name="conference" id="conference" className="form-select" value={this.state.conference}>
                      <option value="">Choose a Conference</option>
                      {this.state.conferences.map(conference => {
                          return (
                            <option key={conference.id} value={conference.id}>
                                {conference.name}
                            </option>
                          );
                      })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
    }

}

export default PresentationForm;