window.addEventListener('DOMContentLoaded', async () => {
  const url = "http://localhost:8000/api/conferences/";
  const response = await fetch(url);
  if (response.ok) {
    const data = await response.json();
    // console.log(data);
    const selectTag = document.getElementById("conference");

    for (let conference of data.conferences) {
      const conferenceId = conference.id
      const conferenceName = conference.name
      // console.log(conference);
      const optionTag = document.createElement("option");
      optionTag.value = conferenceId;
      optionTag.innerHTML = conferenceName;
      selectTag.appendChild(optionTag);
    }
  };

  const selectTag = document.getElementById("conference");
  selectTag.addEventListener('change', async function () {
    const conferenceId = selectTag.value;

    const formTag = document.getElementById('create-presentation-form');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();

      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));

      const presentationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;
      const fetchConfig = {
        method: "post",
        body: json,
        headers: {
          'Content-Type': 'application/json',
        },
      };

      const response = await fetch(presentationUrl, fetchConfig);
      if (response.ok) {
        formTag.reset();
        const newPresentation = await response.json();
        console.log(newPresentation);
      }
    });
  });
});