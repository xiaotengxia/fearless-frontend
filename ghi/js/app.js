function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
        <div class="col-sm-6 col-lg-4 mb-4" style="page-break-inside: avoid;">
            <div class="card shadow p-3">
                <img src="${pictureUrl}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">${name}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                    <p class="card-text">${description}</p>
                </div>
                <div class="card-footer text-muted">
                    ${starts} - ${ends}
                </div>
            </div>
        </div>
    `;
  }

function alertText() {
    return `
        <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Oops!</h4>
            <p>The response is bad!</p>
            <hr>
            <p class="mb-0">Whenever you need to, be sure to use margin utilities to keep things nice and tidy.</p>
        </div>
    `;
    }

function alertCatch() {
return `
    <div class="alert alert-warning" role="alert">
        <h4 class="alert-heading">Oops!</h4>
        <p>An error is raised!</p>
        <hr>
        <p class="mb-0">Whenever you need to, be sure to use margin utilities to keep things nice and tidy.</p>
    </div>
`;
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try{
        const response = await fetch(url);

        if (!response.ok) {
            // Figure out what to do when the response is bad
            const html = alertText();
            const bodys = document.querySelector('main');
            bodys.innerHTML = html;
            
        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                // console.log(detailResponse);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    // console.log(details);
                    const description = details.conference.description;
                    const name = details.conference.name;
                    const pictureUrl = details.conference.location.picture_url;
                    const starts = new Date(details.conference.starts).toLocaleDateString()
                    const ends = new Date(details.conference.ends).toLocaleDateString();
                    const location = details.conference.location.name;
                    const html = createCard(name, description, pictureUrl, starts, ends, location);
                    
                    // const column = document.querySelector('.row');
                    const column = document.getElementById('container');
                    column.innerHTML += html;
                    
                }  
            }
        };
    } catch (e) {
        // Figure out what to do if an error is raised
        const html = alertCatch();
        const bodys = document.querySelector('main');
        bodys.innerHTML = html;
    }

});
