window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
    //   console.log(data)
  
      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

    // Here, add the 'd-none' class to the loading icon
    const loadingTag = document.getElementById('loading');
    loadingTag.classList.add("d-none");
    // Here, remove the 'd-none' class from the select tag
    selectTag.classList.remove("d-none");
    }


    selectTag.addEventListener('change', async function () {
        const conferenceUrl = selectTag.value;
        // console.log(conferenceId)

        const formTag = document.getElementById('create-attendee-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();

            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));

            const presentationUrl = `http://localhost:8001${conferenceUrl}attendees/`;
            
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                'Content-Type': 'application/json',
                },
            };

            const response = await fetch(presentationUrl, fetchConfig);
            if (response.ok) {
                // formTag.reset();

                const loadingMessate = document.getElementById('success-message');
                loadingMessate.classList.remove("d-none");
                formTag.classList.add("d-none");
            }
        });
    })
  });